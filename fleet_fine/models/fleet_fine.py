# -*- encoding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp import models, fields, api


class FleetFine(models.Model):
    _name = "fleet.fine"

    fine_date = fields.Date(string="Fecha")
    fleet_id = fields.Many2one(string="Vehiculo", comodel_name="fleet.vehicle")
    license_plate = fields.Char(string="Matricula", related="fleet_id.license_plate")
    driver = fields.Many2one(string="Conductor", comodel_name="hr.employee")
    location = fields.Many2one(comodel_name="res.better.zip", string="Ubicacion de multa")
    more_data = fields.Text(string="Mas datos")
